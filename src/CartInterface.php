<?php

namespace code2magic\cart;

use yii\base\Arrayable;

/**
 * Interface CartInterface.
 */
interface CartInterface extends Arrayable, \JsonSerializable
{
    /** Prepare positions */
    const EVENT_PREPARE_POSITIONS = 'preparePositions';

    /** Triggered on position put */
    const EVENT_POSITION_PUT = 'putPosition';

    /** Triggered on position update */
    const EVENT_POSITION_UPDATE = 'updatePosition';

    /** Triggered on after position remove */
    const EVENT_BEFORE_POSITION_REMOVE = 'removePosition';

    /** Triggered on any cart change: add, update, delete position */
    const EVENT_CART_CHANGE = 'cartChange';

    /** Triggered on after cart cost calculation */
    const EVENT_COST_CALCULATION = 'cartCostCalculation';

    /** Apply cart discount */
    const EVENT_APPLY_DISCOUNT = 'cartApplyDiscount';

    /**
     * Shopping cart ID to support multiple carts.
     *
     * @var string
     */
    public function getId();

    /**
     * @param CartPositionInterface $position
     * @param int                   $quantity
     */
    public function put($position, $quantity = 1);

    /**
     * @param CartPositionInterface $position
     * @param int                   $quantity
     */
    public function update($position, $quantity);

    /**
     * Removes position from the cart.
     *
     * @param CartPositionInterface $position
     */
    public function remove($position);

    /**
     * Removes position from the cart by ID.
     *
     * @param string $id
     */
    public function removeById($id);

    /**
     * Remove all positions.
     */
    public function removeAll();

    /**
     * Returns position by it's id. Null is returned if position was not found.
     *
     * @param string $id
     *
     * @return CartPositionInterface|null
     */
    public function getPositionById($id);

    /**
     * Checks whether cart position exists or not.
     *
     * @param string $id
     *
     * @return bool
     */
    public function hasPosition($id);

    /**
     * @return CartPositionInterface[]
     */
    public function getPositions();

    /**
     * @param CartPositionInterface[] $positions
     */
    public function setPositions($positions);

    /**
     * Returns true if cart is empty.
     *
     * @return bool
     */
    public function getIsEmpty();

    /**
     * @return int
     */
    public function getCount();

    /**
     * Return full cart cost as a sum of the individual positions costs.
     *
     * @return int
     */
    public function getCost();

    /**
     * Return full cart cost as a sum of the individual positions costs with discount.
     *
     * @return int
     */
    public function getCostWithDiscount();

    /**
     * Returns hash (md5) of the current cart, that is unique to the current combination
     * of positions, quantities and costs. This helps us fast compare if two carts are the same, or not, also
     * we can detect if cart is changed (comparing hash to the one's saved somewhere).
     *
     * @return string
     */
    public function getHash();

    /**
     * @param array|null $attributeNames
     * @param bool       $clearErrors
     *
     * @return bool
     */
    public function validate($attributeNames = null, $clearErrors = true);

    /**
     * @param string|null $attribute
     *
     * @return array
     */
    public function getErrors($attribute = null);
}
