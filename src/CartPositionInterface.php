<?php

namespace code2magic\cart;

use yii\base\Arrayable;

/**
 * Interface CartPositionInterface.
 */
interface CartPositionInterface extends Arrayable, \JsonSerializable
{
    /**
     * Triggered on price calculation.
     */
    const EVENT_PRICE_CALCULATION = 'cartPositionPriceCalculation';

    /**
     * Apply cart position discount.
     */
    const EVENT_APPLY_DISCOUNT = 'cartPositionApplyDiscount';

    /**
     * @return int|float
     */
    public function getPrice();

    /**
     * @return int|float
     */
    public function getPriceWithDiscount();

    /**
     * @return int|float
     */
    public function getCost();

    /**
     * @return int|float
     */
    public function getCostWithDiscount();

    /**
     * @return string
     */
    public function getId();

    /**
     * @return int|float
     */
    public function getQuantity();

    /**
     * @param int|float $quantity
     */
    public function setQuantity($quantity);

    /**
     * @param array|null $attributeNames
     * @param bool       $clearErrors
     *
     * @return bool
     */
    public function validate($attributeNames = null, $clearErrors = true);

    /**
     * @param string|null $attribute
     *
     * @return array
     */
    public function getErrors($attribute = null);
}
