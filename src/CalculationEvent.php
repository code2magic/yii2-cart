<?php

namespace code2magic\cart;

/**
 * Class CalculationEvent.
 */
class CalculationEvent extends \yii\base\Event
{
    /**
     * Cost of the cart or position, that was calculated.
     *
     * @var int|float
     */
    public $cost;
}
