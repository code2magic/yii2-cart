<?php

namespace code2magic\cart;

use code2magic\core\traits\ArrayableTrait;
use yii\di\Instance;

/**
 * Class CartPositionTrait.
 *
 * @see CartPositionInterface
 */
trait CartPositionTrait
{
    use ArrayableTrait;

    /**
     * @var int|float
     */
    protected $_quantity;

    /**
     * @return int|float
     */
    public function getQuantity()
    {
        return $this->_quantity;
    }

    /**
     * @param int|float $quantity
     */
    public function setQuantity($quantity)
    {
        $this->_quantity = $quantity;
    }

    /**
     * @return int|float
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function getPrice()
    {
        /**
         * @var CalculationEvent $calculationEvent
         */
        $calculationEvent = Instance::ensure(
            [
                'cost' => $this->getPriceInternal(),
            ],
            CalculationEvent::class
        );
        $this->trigger(CartPositionInterface::EVENT_PRICE_CALCULATION, $calculationEvent);

        return max(0, $calculationEvent->cost);
    }

    /**
     * @return int|float
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function getPriceWithDiscount()
    {
        /**
         * @var CalculationEvent $calculationEvent
         */
        $calculationEvent = Instance::ensure(
            [
                'cost' => $this->getPrice(),
            ],
            CalculationEvent::class
        );
        $this->trigger(CartPositionInterface::EVENT_APPLY_DISCOUNT, $calculationEvent);

        return max(0, $calculationEvent->cost);
    }

    /**
     * @return int|float
     */
    abstract protected function getPriceInternal();

    /**
     * @return mixed
     */
    abstract public function getId(): string;

    /**
     * @return int|float
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function getCost()
    {
        return max(0, $this->getPrice() * $this->getQuantity());
    }

    /**
     * @return int|float
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function getCostWithDiscount()
    {
        return max(0, $this->getPriceWithDiscount() * $this->getQuantity());
    }

    /**
     * @inheritDoc
     */
    public function fields()
    {
        return [
            'id' => 'id',
            'price' => 'price',
            'price_with_discount' => 'priceWithDiscount',
            'quantity' => 'quantity',
            'cost' => 'cost',
            'cost_with_discount' => 'costWithDiscount',
        ];
    }
}
