<?php

namespace code2magic\cart;

/**
 * Class InlineDiscount.
 */
class InlineDiscountBehavior extends \yii\base\Behavior
{
    /**
     * {@inheritdoc}
     */
    public function events()
    {
        return [
            Cart::EVENT_APPLY_DISCOUNT => 'onApplyDiscount',
            CartPositionInterface::EVENT_APPLY_DISCOUNT => 'onApplyDiscount',
        ];
    }

    /**
     * @var callable
     */
    public $discountCalculation;

    /**
     * @param CalculationEvent $event
     *
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function onApplyDiscount($event)
    {
        $event->cost = \Yii::$container->invoke($this->discountCalculation, [$event]);
    }
}
