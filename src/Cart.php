<?php

namespace code2magic\cart;

use code2magic\core\traits\ArrayableTrait;
use yii\base\Component;
use yii\di\Instance;
use yii\helpers\ArrayHelper;

/**
 * Class Cart.
 */
class Cart extends Component implements CartInterface
{
    /**
     * Shopping cart ID to support multiple carts.
     *
     * @var string
     */
    public $cartId;

    /**
     * @var array
     */
    protected $_errors = [];

    /**
     * @var CartPositionInterface[]
     */
    protected $_positions = [];

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->cartId;
    }

    /**
     * {@inheritdoc}
     */
    public function put($position, $quantity = 1)
    {
        if (isset($this->_positions[$position->getId()])) {
            $this->_positions[$position->getId()]->setQuantity(
                $this->_positions[$position->getId()]->getQuantity() + $quantity
            );
        } else {
            $position->setQuantity($quantity);
            $this->_positions[$position->getId()] = $position;
        }
        /**
         * @var CartActionEvent $cartActionEvent
         */
        $cartActionEvent = Instance::ensure(
            [
                'action' => CartActionEvent::ACTION_POSITION_PUT,
                'positions' => [$this->_positions[$position->getId()]],
            ],
            CartActionEvent::class
        );
        $this->trigger(
            self::EVENT_POSITION_PUT,
            $cartActionEvent
        );
        /**
         * @var CartActionEvent $cartActionEvent
         */
        $cartActionEvent = Instance::ensure(
            [
                'action' => CartActionEvent::ACTION_POSITION_PUT,
                'positions' => [$this->_positions[$position->getId()]],
            ],
            CartActionEvent::class
        );
        $this->trigger(
            self::EVENT_CART_CHANGE,
            $cartActionEvent
        );
    }

    /**
     * {@inheritdoc}
     */
    public function update($position, $quantity)
    {
        if ($quantity <= 0) {
            $this->remove($position);

            return;
        }
        if (isset($this->_positions[$position->getId()])) {
            $this->_positions[$position->getId()]->setQuantity($quantity);
        } else {
            $position->setQuantity($quantity);
            $this->_positions[$position->getId()] = $position;
        }
        /**
         * @var CartActionEvent $cartActionEvent
         */
        $cartActionEvent = Instance::ensure(
            [
                'action' => CartActionEvent::ACTION_UPDATE,
                'positions' => [$this->_positions[$position->getId()]],
            ],
            CartActionEvent::class
        );
        $this->trigger(
            self::EVENT_POSITION_UPDATE,
            $cartActionEvent
        );
        /**
         * @var CartActionEvent $cartActionEvent
         */
        $cartActionEvent = Instance::ensure(
            [
                'action' => CartActionEvent::ACTION_UPDATE,
                'positions' => [$this->_positions[$position->getId()]],
            ],
            CartActionEvent::class
        );
        $this->trigger(
            self::EVENT_CART_CHANGE,
            $cartActionEvent
        );
    }

    /**
     * {@inheritdoc}
     */
    public function remove($position)
    {
        $this->removeById($position->getId());
    }

    /**
     * {@inheritdoc}
     */
    public function removeById($id)
    {
        /**
         * @var CartActionEvent $cartActionEvent
         */
        $cartActionEvent = Instance::ensure(
            [
                'action' => CartActionEvent::ACTION_BEFORE_REMOVE,
                'position' => $this->getPositionById($id),
            ],
            CartActionEvent::class
        );
        $this->trigger(
            self::EVENT_BEFORE_POSITION_REMOVE,
            $cartActionEvent
        );
        ArrayHelper::remove($this->_errors['positions'], $id);
        /**
         * @var CartActionEvent $cartActionEvent
         */
        $cartActionEvent = Instance::ensure(
            [
                'action' => CartActionEvent::ACTION_BEFORE_REMOVE,
                'positions' => [ArrayHelper::remove($this->_positions, $id)],
            ],
            CartActionEvent::class
        );
        $this->trigger(
            self::EVENT_CART_CHANGE,
            $cartActionEvent
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getPositionById($id)
    {
        return $this->hasPosition($id) ? $this->_positions[$id] : null;
    }

    /**
     * {@inheritdoc}
     */
    public function hasPosition($id)
    {
        return isset($this->_positions[$id]);
    }

    /**
     * {@inheritdoc}
     */
    public function removeAll()
    {
        /**
         * @var CartActionEvent $cartActionEvent
         */
        $cartActionEvent = Instance::ensure(
            [
                'action' => CartActionEvent::ACTION_REMOVE_ALL,
                'positions' => $this->_positions,
            ],
            CartActionEvent::class
        );
        $this->_positions = [];
        unset($this->_errors['positions']);
        $this->trigger(
            self::EVENT_CART_CHANGE,
            $cartActionEvent
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getIsEmpty()
    {
        return 0 == count($this->_positions);
    }

    /**
     * {@inheritdoc}
     */
    public function getCount()
    {
        $count = 0;
        foreach ($this->_positions as $position) {
            $count += $position->getQuantity();
        }

        return $count;
    }

    /**
     * {@inheritdoc}
     */
    public function getCost()
    {
        $cost = 0;
        foreach ($this->_positions as $position) {
            $cost += $position->getCost();
        }
        /**
         * @var CalculationEvent $calculationEvent
         */
        $calculationEvent = Instance::ensure(
            [
                'cost' => $cost,
            ],
            CalculationEvent::class
        );
        $this->trigger(self::EVENT_COST_CALCULATION, $calculationEvent);

        return max(0, $calculationEvent->cost);
    }

    /**
     * {@inheritdoc}
     */
    public function getCostWithDiscount()
    {
        $cost = 0;
        foreach ($this->_positions as $position) {
            $cost += $position->getCostWithDiscount();
        }
        /**
         * @var CalculationEvent $calculationEvent
         */
        $calculationEvent = Instance::ensure(
            [
                'cost' => $cost,
            ],
            CalculationEvent::class
        );
        $this->trigger(self::EVENT_APPLY_DISCOUNT, $calculationEvent);

        return max(0, $calculationEvent->cost);
    }

    /**
     * {@inheritdoc}
     */
    public function getHash()
    {
        $data = [];
        foreach ($this->getPositions() as $position) {
            $data[] = [$position->getId(), $position->getQuantity(), $position->getPrice()];
        }

        return md5(serialize($data));
    }

    /**
     * {@inheritdoc}
     */
    public function getPositions()
    {
        return $this->_positions ?: [];
    }

    /**
     * {@inheritdoc}
     * @throws \yii\base\InvalidConfigException
     */
    public function setPositions($positions)
    {
        $this->_positions = $this->preparePositions($positions);
        $this->validate();
        /**
         * @var CartActionEvent $cartActionEvent
         */
        $cartActionEvent = Instance::ensure(
            [
                'action' => CartActionEvent::ACTION_SET_POSITIONS,
                'positions' => $this->_positions,
            ],
            CartActionEvent::class
        );
        $this->trigger(
            self::EVENT_CART_CHANGE,
            $cartActionEvent
        );
    }

    /**
     * @param array $positions CartPosition list or array
     * @return CartPositionInterface[]
     * @throws \yii\base\InvalidConfigException
     */
    protected function preparePositions(array $positions)
    {
        $output = [];
        foreach ($positions as $position) {
            $output[] = Instance::ensure($position, CartPositionInterface::class);
        }
        /**
         * @var CartActionEvent $cartActionEvent
         */
        $cartActionEvent = Instance::ensure(
            [
                'action' => CartActionEvent::ACTION_PREPARE_POSITIONS,
                'positions' => $output,
            ],
            CartActionEvent::class
        );
        $this->trigger(
            self::EVENT_PREPARE_POSITIONS,
            $cartActionEvent
        );

        return $cartActionEvent->positions;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $this->validate(null, false);
        $output = [
            'positions' => ArrayHelper::getColumn(
                $this->getPositions(),
                function ($position) {
                    return $position->jsonSerialize();
                },
            ),
        ];
        if (!empty($this->getId())) {
            ArrayHelper::setValue($output, ['id'], $this->getId());
        }
        if (!empty($this->getErrors())) {
            ArrayHelper::setValue($output, ['errors'], $this->getErrors());
        }

        return $output;
    }

    /**
     * @param array|null $attributeNames
     * @param bool       $clearErrors
     *
     * @return bool
     */
    public function validate($attributeNames = null, $clearErrors = true)
    {
        if ($clearErrors) {
            $this->_errors = [];
        }
        foreach ($this->getPositions() as $position) {
            if (!$position->validate()) {
                ArrayHelper::setValue($this->_errors, ['positions', $position->getId()], $position->getErrors());
                unset($this->_positions[$position->getId()]);
            }
        }

        return 0 === count($this->_errors);
    }

    /**
     * @param string|null $attribute
     *
     * @return array
     */
    public function getErrors($attribute = null)
    {
        return $this->_errors ?: [];
    }

    //<editor-fold desc="Arrayable">

    use ArrayableTrait;

    /**
     * @inheritDoc
     */
    public function fields()
    {
        return [
            'positions' => 'positions',
            'count' => 'count',
            'cost' => 'cost',
            'cost_with_discount' => 'costWithDiscount',
        ];
    }

    //</editor-fold>
}
