<?php
/**
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
return [
    'container' => [
        'definitions' => [
            \code2magic\cart\CartInterface::class => [
                '__class' => \code2magic\cart\Cart::class,
            ],
        ],
        'singletons' => [
        ],
    ],
];
